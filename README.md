# Information / Информация

Выравнивание текста в статье.

## Install / Установка

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_Align`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_Align' );
```

## Syntax / Синтаксис

```html
<align id="[text|image]" type="[left|center|right]">[CONTENT]</align>
```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
